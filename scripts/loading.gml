var loadlist, loadstr, file;
loadlist = ds_list_create();
//is there a save?
if (!file_exists("score.flappy")) return -1
//open file
file = file_text_open_read("score.flappy");
loadstr = file_text_read_string(file);
file_text_close(file);
ds_list_read(loadlist, loadstr);
//values loaded
global.score = ds_list_find_value(loadlist, 0);
ds_list_delete(loadlist, 0);

global.highscore = ds_list_find_value(loadList,0);
ds_list_delete(loadList,0);

//ender
ds_list_destroy(loadlist);
